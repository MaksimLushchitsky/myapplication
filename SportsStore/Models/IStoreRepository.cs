﻿using System.Linq;

public interface IStoreRepository 
{
    IQueryable<Product> Products { get; }
}