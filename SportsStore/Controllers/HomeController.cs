﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sport_Store.Models;

namespace Sport_Store.Controllers
{
    public class HomeController : Controller 
    {
          private readonly IStoreRepository repository;

          public int PageSize = 4;

          public HomeController(IStoreRepository repo)
          {
              repository = repo;
          }

          public ViewResult Index(string category, int productPage = 1)
                     => View(new ProductsListViewModel
                        {
                            Products = repository.Products
                               .Where(p => category == null || p.Category == category)
                               .OrderBy(p => p.ProductID)
                               .Skip((productPage - 1) * PageSize)
                               .Take(PageSize),
                            PagingInfo = new PagingInfo {
                               CurrentPage = productPage,
                               ItemsPerPage = PageSize,
                               TotalItems = category == null ?
                                   repository.Products.Count() :
                                   repository.Products.Where(e =>
                                       e.Category == category).Count()
                           },
                           CurrentCategory = category
                        });

    }


}