using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Sport_Store
{
    public class Startup 
    {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) 
        {
            app.UseDeveloperExceptionPage();
            app.UseStatusCodePages();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
              {
                  endpoints.MapControllerRoute("catpage",
                      "{category}/Page{productPage:int}",
                      new { Controller = "Home", action = "Index" });
                  endpoints.MapControllerRoute("page", "Page{productPage:int}",
                      new { Controller = "Home", action = "Index", productPage = 1 });
                  endpoints.MapControllerRoute("category", "{category}",
                      new { Controller = "Home", action = "Index", productPage = 1 });
                  endpoints.MapControllerRoute("pagination",
                      "Products/Page{productPage}",
                      new { Controller = "Home", action = "Index", productPage = 1 });
                  endpoints.MapDefaultControllerRoute();
              });
              SeedData.EnsurePopulated(app);
        }
        public Startup(IConfiguration config) 
        {
            Configuration = config;
        }

        private IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services) 
        {
            dotnet ef migrations add Initial
            services.AddScoped<IStoreRepository, EFStoreRepository>();
            services.AddScoped<IOrderRepository, EFOrderRepository>();
            services.AddControllersWithViews();

            services.AddDbContext<StoreDbContext>(opts => {
                opts.UseSqlServer(
                    Configuration["ConnectionStrings:SportsStoreConnection"]);
            });

            services.AddDistributedMemoryCache();
            services.AddSession();
        }
        
        

    }

}